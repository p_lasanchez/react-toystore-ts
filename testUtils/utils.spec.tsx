/* eslint-disable import/no-extraneous-dependencies */
import React, { FunctionComponent } from 'react'
import { Provider, ConnectedComponentClass } from 'react-redux'
import configureStore, { MockStore } from 'redux-mock-store'
import thunk from 'redux-thunk'
import { MemoryRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import TestRenderer from 'react-test-renderer'
import theme from '../src/theme'

export const mockFocusedElement: TestRenderer.TestRendererOptions = {
  createNodeMock: (element: React.ReactElement) => {
    if (element.type === 'input') {
      // mock a focus function
      return {
        focus: () => jest.fn(),
      }
    }
    return null
  },
}
export type MountRenderer = {
  _renderer: TestRenderer.ReactTestRenderer,
  _store: MockStore
}

type ComponentType = React.FunctionComponent | ConnectedComponentClass<FunctionComponent<{}>, Pick<{}, never>>

export const mountWithStoreAndRouter = <I, >(initialState: I, Component: ComponentType): MountRenderer => {
  const mockStore = configureStore([thunk])
  const store: MockStore = mockStore(initialState)
  return {
    _renderer: TestRenderer.create(
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <MemoryRouter>
            <Component />
          </MemoryRouter>
        </ThemeProvider>
      </Provider>,
      mockFocusedElement,
    ),
    _store: store,
  }
}

export const mountWithStore = <I, >(initialState: I, Component: ComponentType): MountRenderer => {
  const mockStore = configureStore([thunk])
  const store: MockStore = mockStore(initialState)
  return {
    _renderer: TestRenderer.create(
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <Component />
        </ThemeProvider>
      </Provider>,
      mockFocusedElement,
    ),
    _store: store,
  }
}
