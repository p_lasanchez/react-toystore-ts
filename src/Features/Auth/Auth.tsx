import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { connectAction, closeAction } from '../../Store/authReducer/auth.actions'

import AuthRedirect from './components/AuthRedirect'
import AuthForm from './components/AuthForm'
import { RootState } from '../../Store/types'

interface MapProps {
  isConnected: boolean
  isInvalid: boolean
}
interface MapDispatch {
  auth: (user: string, pass: string) => void
  close: () => void
}

type Props = MapProps & MapDispatch

const propTypes: React.WeakValidationMap<Props> = {
  isConnected: PropTypes.bool.isRequired,
  isInvalid: PropTypes.bool.isRequired,
  auth: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
}

const Auth: React.FunctionComponent<Props> = ({
  isConnected, isInvalid, auth, close,
}: Props): JSX.Element => {
  const handleSubmit = (email: string, pass: string) => {
    auth(email, pass)
  }

  return isConnected ? <AuthRedirect close={close} />
    : (
      <AuthForm
        close={close}
        isInvalid={isInvalid}
        submit={handleSubmit}
      />
    )
}

Auth.propTypes = propTypes

const mapStateToProps = ({ authReducer }: RootState): MapProps => ({
  isConnected: authReducer.isConnected,
  isInvalid: authReducer.isInvalid,
})

export default connect(mapStateToProps, {
  auth: connectAction,
  close: closeAction,
} as MapDispatch)(Auth)
