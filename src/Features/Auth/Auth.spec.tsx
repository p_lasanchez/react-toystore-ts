/* eslint-disable import/no-extraneous-dependencies */
import React, { FunctionComponent } from 'react'
import { MockStore } from 'redux-mock-store'

import { act, ReactTestRenderer } from 'react-test-renderer'
import { mountWithStoreAndRouter } from '../../../testUtils/utils.spec'
import * as services from '../../Services/api.service'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'

import Auth from './Auth'
import AuthRedirect from './components/AuthRedirect'
import AuthForm from './components/AuthForm'
import { AUTH } from '../../Store/authReducer/types'

jest.mock('./components/AuthRedirect', () => (
  function MockComponent() { return <div /> }
))
jest.mock('./components/AuthForm', () => (
  function MockComponent() { return <div /> }
))

jest.mock('../../Services/api.service', () => ({
  authentication: jest.fn(() => Promise.resolve({ data: [] }))
}))

describe('<Auth />', () => {
  let store: MockStore
  let renderer: ReactTestRenderer

  afterEach(() => {
    renderer.unmount()
    store.clearActions()
  })

  function initRenderer({ isConnected, isInvalid }: { isConnected: boolean, isInvalid: boolean}) {
    const { _renderer, _store } = mountWithStoreAndRouter(
      {
        authReducer: { ...defaultAuthState, isConnected, isInvalid },
        toyListReducer: { ...defaultToyListState },
      },
      Auth as unknown as FunctionComponent
    )
    renderer = _renderer
    store = _store
  }

  it('should render with redirect', () => {
    act(() => initRenderer({ isConnected: true, isInvalid: false }))

    const instance = renderer.root
    const redirect = instance.findByType(AuthRedirect)

    redirect.props.close()

    expect(store.getActions()).toContainEqual({
      type: AUTH.CLOSE,
    })
  })

  it('should render with form', (done) => {
    act(() => initRenderer({ isConnected: false, isInvalid: true }))

    const instance = renderer.root
    const form = instance.findByType(AuthForm)

    expect(form.props.isInvalid).toBe(true)

    form.props.close()
    expect(store.getActions()).toContainEqual({
      type: AUTH.CLOSE,
    })

    form.props.submit('toto', 'pan')

    setTimeout(() => {
      expect(services.authentication).toBeCalledWith('toto', 'pan')
      expect(store.getActions()).toContainEqual({
        type: AUTH.CONNECT_ERROR,
      })

      done()
    })
  })
})
