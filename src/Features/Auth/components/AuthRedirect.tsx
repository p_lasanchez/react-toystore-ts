import React, { useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

interface Props {
  close: () => void
}

const propTypes = {
  close: PropTypes.func.isRequired,
}

const AuthRedirect: React.FunctionComponent<Props> = ({ close }: Props): JSX.Element => {
  useEffect(() => {
    close()
  }, [close])
  return <Redirect to="/basket" />
}

AuthRedirect.propTypes = propTypes

export default React.memo<Props>(AuthRedirect)
