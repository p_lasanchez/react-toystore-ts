import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

import {
  AuthBackground, AuthBox, AuthInput, AuthMessage, AuthButton, AuthTitle,
} from './styled'

interface Props {
  close: () => void
  submit: (email: string, pass: string) => void
  isInvalid: boolean
}

const propTypes = {
  close: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
  isInvalid: PropTypes.bool.isRequired,
}
const AuthForm: React.FunctionComponent<Props> = ({ close, submit, isInvalid }: Props): JSX.Element => {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const emailField = useRef<HTMLInputElement>(null)

  useEffect(() => {
    if (emailField && emailField.current) {
      emailField.current.focus()
    }
  }, [])

  const handleChangeMail = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value)
  }

  const handleChangePass = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPass(e.target.value)
  }

  const prevent = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    e.preventDefault()
    e.stopPropagation()
  }

  const handleSubmit = () => {
    submit(email, pass)
  }

  return (
    <AuthBackground onClick={close}>
      <AuthBox onClick={prevent}>
        <AuthTitle>Identification</AuthTitle>
        <form noValidate>
          <AuthInput
            type="email"
            isInvalid={isInvalid}
            value={email}
            onChange={handleChangeMail}
            ref={emailField}
          />
          <AuthInput type="password" isInvalid={isInvalid} value={pass} onChange={handleChangePass} />
          <AuthMessage isVisible={isInvalid}>Identifiants invalides</AuthMessage>
          <AuthButton onClick={handleSubmit}>Connect</AuthButton>
        </form>
      </AuthBox>
    </AuthBackground>
  )
}

AuthForm.propTypes = propTypes

export default React.memo(AuthForm, (prev, next) => prev.isInvalid === next.isInvalid)
