import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  StyledHeaderWrapper, StyledHeaderLink, StyledHeaderBasket,
} from './components/styled'
import { openAction } from '../../Store/authReducer/auth.actions'
import { RootState } from '../../Store/types'
import { ToyType } from '../../Services/types'

interface MapProps {
  nbSelected: number
}
interface MapDispatch {
  open: () => void
}

export type Props = MapProps & MapDispatch

const propTypes: React.WeakValidationMap<Props> = {
  nbSelected: PropTypes.number.isRequired,
  open: PropTypes.func.isRequired,
}

const Header: React.FunctionComponent<Props> = ({ nbSelected, open }: Props) => {

  const handleClick = () => {
    open()
  }

  return (
    <StyledHeaderWrapper>
      <StyledHeaderLink to="/">Toy Store</StyledHeaderLink>
      <StyledHeaderBasket onClick={handleClick}>{nbSelected}</StyledHeaderBasket>
    </StyledHeaderWrapper>
  )
}

Header.propTypes = propTypes
const mapStateToProps = ({ toyListReducer }: RootState) => ({
  nbSelected: toyListReducer.toys.filter((toy: ToyType) => toy.selected).length,
})
export default connect(mapStateToProps, {
  open: openAction,
})(Header)
