/* eslint-disable import/no-extraneous-dependencies */
import { FunctionComponent } from 'react'
import { MockStore } from 'redux-mock-store'
import TestRenderer from 'react-test-renderer'

import { mountWithStoreAndRouter } from '../../../testUtils/utils.spec'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'

import Header, { Props } from './Header'
import { StyledHeaderBasket } from './components/styled'
import { AUTH } from '../../Store/authReducer/types'
import { ToyType } from '../../Services/types'

describe('<Header />', () => {
  let store: MockStore
  let renderer: TestRenderer.ReactTestRenderer

  afterEach(() => {
    store.clearActions()
    renderer.unmount()
  })

  function initRenderer() {
    const { _renderer, _store } = mountWithStoreAndRouter(
      {
        authReducer: defaultAuthState,
        toyListReducer: { ...defaultToyListState, toys: [{ selected: true }] },
      },
      Header as unknown as FunctionComponent,
    )
    renderer = _renderer
    store = _store
  }

  it('should render', () => {
    initRenderer()

    const instance = renderer.root
    const basket = instance.findByType(StyledHeaderBasket)

    basket.props.onClick()
    expect(store.getActions()).toContainEqual({
      type: AUTH.OPEN,
    })

    expect(basket.props.children).toBe(1)
  })
})
