/* eslint-disable import/no-extraneous-dependencies */
import React, { FunctionComponent } from 'react'
import { AxiosResponse } from 'axios'

import TestRenderer, { act } from 'react-test-renderer'
import  { MockStore } from 'redux-mock-store'
import { mountWithStore } from '../../../testUtils/utils.spec'

import { defaultAuthState } from '../../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../../Store/toyListReducer/toyList.reducer'
import * as services from '../../Services/api.service'

import List from './List'
import ToyList from './components/ToyList'
import { TOYS } from '../../Store/toyListReducer/types'
import { ToyType } from '../../Services/types'

jest.mock('./components/ToyList', () => function MockComponent() { return <div /> })

describe('<List />', () => {
  let store: MockStore
  let renderer: TestRenderer.ReactTestRenderer

  afterEach(() => {
    store.clearActions()
    renderer.unmount()
  })

  function initRenderer({ isLoaded }: { isLoaded: boolean }) {
    const { _renderer, _store } = mountWithStore(
      {
        authReducer: defaultAuthState,
        toyListReducer: { ...defaultToyListState, isLoaded },
      },
      List as unknown as FunctionComponent,
    )
    renderer = _renderer
    store = _store
  }

  it('should render', (done) => {
    jest.spyOn(services, 'getToys').mockReturnValue(
      Promise.resolve({ data: [{ title: 'hop' }] as ToyType[] } as AxiosResponse<ToyType[]>)
    )

    act(() => initRenderer({ isLoaded: false }))

    setTimeout(() => {
      expect(services.getToys).toBeCalled()
      expect(store.getActions()).toContainEqual({
        type: TOYS.GET_TOYS_SUCCESS,
        toys:  [{ title: 'hop' }],
      })
      done()
    })
  })

  it('should select a toy', () => {
    jest.spyOn(services, 'getToys').mockReturnValue(
      Promise.resolve({ data: [{ id: 7 }] as ToyType[] } as AxiosResponse<ToyType[]>)
    )

    act(() => initRenderer({ isLoaded: true }))

    const instance = renderer.root
    const list = instance.findByType(ToyList)

    list.props.handleClick(7)
    expect(store.getActions()).toContainEqual({
      type: TOYS.SELECT_TOY,
      id: 7,
    })
  })
})
