import React from 'react'
import PropTypes from 'prop-types'
import { ToyType } from '../../../Services/types'

const toyType: React.WeakValidationMap<ToyType> = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  selected: PropTypes.bool,
}

export default {
  toyType,
}
