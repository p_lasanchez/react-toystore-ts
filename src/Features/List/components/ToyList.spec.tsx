/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import TestRenderer, { act } from 'react-test-renderer'

import ToyList from './ToyList'
import { StyledToyWrapper } from './styled'

jest.mock('./styled', () => ({
  StyledToyWrapper: function MockComponent() { return <div /> },
}))
jest.mock('./Toy', () => function MockComponent() { return <div /> })

describe('<ToyList />', () => {
  it('should render', () => {
    const mockClick = jest.fn()

    let renderer: TestRenderer.ReactTestRenderer
    
    const toys = [
      {
        id: 1, title: 'hop', icon: 'ico', price: 10,
      },
      {
        id: 2, title: 'hop2', icon: 'ico2', price: 20,
      },
    ]
    act(() => {
      renderer = TestRenderer.create(
        <ToyList
          handleClick={mockClick}
          toys={toys}
        />,
      )
    })

    act(() => {
      const instance = renderer.root
      const wrapper = instance.findByType(StyledToyWrapper)
      const els = wrapper.props.children

      expect(els.length).toBe(2)

      els[0].props.handleClick()
      expect(mockClick).toBeCalled()
    })
  })
})
