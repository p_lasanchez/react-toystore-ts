import React from 'react'
import PropTypes from 'prop-types'
import { StyledToyButton } from './styled'
import '../../../../node_modules/@mdi/font/css/materialdesignicons.min.css'

interface Props {
  id: number
  selected: boolean
  icon: string
  handleClick: (id: number) => void
}

const propTypes: React.WeakValidationMap<Props> = {
  id: PropTypes.number.isRequired,
  selected: PropTypes.bool.isRequired,
  icon: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
}

const Toy: React.FunctionComponent<Props> = ({
  handleClick, id, selected, icon,
}: Props): JSX.Element => {
  const iconClass = `mdi mdi-${icon}`

  return (
    <StyledToyButton
      onClick={() => handleClick(id)}
      selected={selected}
    >
      <i className={iconClass} />
    </StyledToyButton>
  )
}

Toy.propTypes = propTypes

export default React.memo(Toy, (prev, next) => prev.selected === next.selected)
