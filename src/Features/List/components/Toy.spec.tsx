/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import TestRenderer, { act, ReactTestRenderer, ReactTestInstance } from 'react-test-renderer'

import Toy from './Toy'
import { StyledToyButton } from './styled'

jest.mock('./styled', () => ({
  StyledToyButton: function MockComponent() { return <div /> },
})) 

describe('<Toy />', () => {
  it('should render', () => {
    const mockClick = jest.fn()

    let renderer: ReactTestRenderer
    let instance: ReactTestInstance

    act(() => {
      renderer = TestRenderer.create(
        <Toy
          handleClick={mockClick}
          id={1}
          icon="image"
          selected
        />,
      )
    })

    act(() => {
      instance = renderer.root
      const button = instance.findByType(StyledToyButton)
      const icone = button.props.children

      expect(button.props.selected).toBe(true)
      expect(icone.props.className).toBe('mdi mdi-image')

      button.props.onClick()
      expect(mockClick).toBeCalledWith(1)
    })
  })
})
