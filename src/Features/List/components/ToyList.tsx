import React from 'react'
import PropTypes from 'prop-types'
import { StyledToyWrapper } from './styled'

import Toy from './Toy'
import types from '../models/toyType'
import { ToyType } from '../../../Services/types'

interface Props {
  toys: ToyType[]
  handleClick: (id: number) => void
}

const propTypes: React.WeakValidationMap<Props> = {
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired as PropTypes.Validator<ToyType[]>,
  handleClick: PropTypes.func.isRequired,
}

const ToyList: React.FunctionComponent<Props> = ({ handleClick, toys }: Props): JSX.Element => (
  <StyledToyWrapper>
    {
      toys.map((toy: ToyType) => (
        <Toy key={toy.id} handleClick={handleClick} id={toy.id} selected={!!toy.selected} icon={toy.icon} />)
      )
    }
  </StyledToyWrapper>
)

ToyList.propTypes = propTypes

export default ToyList
