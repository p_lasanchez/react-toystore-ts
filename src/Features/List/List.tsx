import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { getToysAction, selectToyAction } from '../../Store/toyListReducer/toyList.actions'
import types from './models/toyType'

import ToyList from './components/ToyList'
import { ToyType } from '../../Services/types'
import { RootState } from '../../Store/types'

interface MapProps {
  isLoaded: boolean
  toys: ToyType[]
}

interface MapDispatch {
  selectToy: (id: number) => void
  getToys: () => void
}

type Props = MapProps & MapDispatch

const propTypes: React.WeakValidationMap<Props> = {
  isLoaded: PropTypes.bool.isRequired,
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired as PropTypes.Validator<ToyType[]>,
  selectToy: PropTypes.func.isRequired,
  getToys: PropTypes.func.isRequired,
}

const List: React.FunctionComponent<Props> = ({
  isLoaded, toys, getToys, selectToy,
}: Props): JSX.Element => {
  useEffect(() => {
    if (isLoaded) return
    getToys()
  }, [isLoaded, getToys])

  const handleClick = (id: number) => selectToy(id)

  return <ToyList toys={toys} handleClick={handleClick} />
}

List.propTypes = propTypes

const mapStateToProps = ({ toyListReducer }: RootState): MapProps => ({
  isLoaded: toyListReducer.isLoaded,
  toys: toyListReducer.toys,
})

const ConnectedList = connect(mapStateToProps, {
  getToys: getToysAction,
  selectToy: selectToyAction,
})(List)

export default ConnectedList
