/* eslint-disable import/no-extraneous-dependencies */
import { ConnectedComponentClass } from 'react-redux'
import { FunctionComponent } from 'react'
import { act, ReactTestRenderer } from 'react-test-renderer'
import { MockStore } from 'redux-mock-store'

import { mountWithStore } from '../../testUtils/utils.spec'

import { defaultAuthState } from '../Store/authReducer/auth.reducer'
import { defaultToyListState } from '../Store/toyListReducer/toyList.reducer'

import Main from './Main'
import Auth from './Auth/Auth'

jest.mock('axios', () => ({
  get: () => Promise.resolve('done'),
}))

describe('<Main />', () => {
  let store: MockStore
  let renderer: ReactTestRenderer

  afterEach(() => {
    renderer.unmount()
    store.clearActions()
  })

  function initRenderer({ isOpened }: { isOpened: boolean }) {
    const { _renderer, _store } = mountWithStore({
      authReducer: { ...defaultAuthState, isOpened },
      toyListReducer: { ...defaultToyListState },
    }, Main as ConnectedComponentClass<FunctionComponent<{}>, Pick<{}, never>>)

    renderer = _renderer
    store = _store
  }

  it('should render with auth', () => {
    act(() => initRenderer({ isOpened: true }))

    const instance = renderer.root
    const auth = instance.findByType(Auth)
    expect(auth).toBeDefined()
  })

  it('should render without auth', () => {
    act(() => initRenderer({ isOpened: false }))

    const instance = renderer.root
    const auth = instance.findAllByType(Auth)
    expect(auth.length).toBe(0)
  })
})
