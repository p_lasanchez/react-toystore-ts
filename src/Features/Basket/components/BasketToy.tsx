import React from 'react'
import PropTypes from 'prop-types'

import { StyledToyWrapper, StyledToyText } from './styled'

import '../../../../node_modules/@mdi/font/css/materialdesignicons.min.css'

import types from '../../List/models/toyType'
import { ToyType } from '../../../Services/types'

interface Props {
  handleClick: (id: number) => void
  toy: ToyType
}

const propTypes: React.WeakValidationMap<Props> = {
  toy: PropTypes.shape(types.toyType).isRequired,
  handleClick: PropTypes.func.isRequired,
}

const BasketToy = ({ toy, handleClick }: Props) => (
  <StyledToyWrapper onClick={() => handleClick(toy.id)}>
    <StyledToyText>
      <span>{toy.title}</span>
      &nbsp;&bull;&nbsp;
      <span>
        {toy.price}
        &euro;
      </span>
    </StyledToyText>
    <i className="mdi mdi-close" />
  </StyledToyWrapper>
)

BasketToy.propTypes = propTypes

export default BasketToy
