import React from 'react'
import PropTypes from 'prop-types'

import BasketToy from './BasketToy'
import { StyledToyColumn } from './styled'

import types from '../../List/models/toyType'
import { ToyType } from '../../../Services/types'

interface Props {
  handleClick: (id: number) => void
  toys: ToyType[]
}

const propTypes: React.WeakValidationMap<Props> = {
  toys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired as PropTypes.Validator<ToyType[]>,
  handleClick: PropTypes.func.isRequired,
}

const BasketList = ({
  handleClick, toys,
}: Props) => (
  <StyledToyColumn>
    {toys.map(toy => <BasketToy key={toy.id} handleClick={handleClick} toy={toy} />)}
  </StyledToyColumn>
)


BasketList.propTypes = propTypes

export default BasketList
