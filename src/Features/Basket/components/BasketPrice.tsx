import React from 'react'
import PropTypes from 'prop-types'
import { StyledPrice, StyledToyColumn } from './styled'

interface Props {
  price: number
}

const propTypes: React.WeakValidationMap<Props> = {
  price: PropTypes.number.isRequired,
}

const BasketPrice = ({ price }: Props) => (
  <StyledToyColumn>
    <StyledPrice>
      {price}
      &euro;
    </StyledPrice>
  </StyledToyColumn>
)

BasketPrice.propTypes = propTypes

export default BasketPrice
