import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { selectToyAction } from '../../Store/toyListReducer/toyList.actions'
import types from '../List/models/toyType'

import BasketList from './components/BasketList'
import BasketPrice from './components/BasketPrice'

import { StyledBasketWrapper } from './components/styled'
import { ToyType } from '../../Services/types'
import { RootState } from '../../Store/types'

interface MapProps {
  selectedToys: ToyType[],
  price: number
}

interface MapDispatch {
  selectToy: (id: number) => void
}

export type Props = MapProps & MapDispatch

const propTypes: React.WeakValidationMap<Props> = {
  selectedToys: PropTypes.arrayOf(PropTypes.shape(types.toyType)).isRequired as PropTypes.Validator<ToyType[]>,
  selectToy: PropTypes.func.isRequired,
  price: PropTypes.number.isRequired,
}

const Basket = ({ selectedToys, price, selectToy }: Props) => {
  const handleClick = (id: number) => selectToy(id)

  return (
    <StyledBasketWrapper>
      <BasketList toys={selectedToys} handleClick={handleClick} />
      <BasketPrice price={price} />
    </StyledBasketWrapper>
  )
}

Basket.propTypes = propTypes

const mapStateToProps = ({ toyListReducer }: RootState): MapProps => ({
  selectedToys: toyListReducer.toys.filter((toy: ToyType) => toy.selected),
  price: toyListReducer.toys
    .filter((toy: ToyType) => toy.selected)
    .reduce((totalPrice: number, toy: ToyType) => (
      totalPrice + toy.price
    ), 0),
})

export default connect(mapStateToProps, {
  selectToy: selectToyAction,
})(Basket)
