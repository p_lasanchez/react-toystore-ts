export interface UserType {
  id: number
  user: string
  pass: string
}

export interface ToyType {
  id: number
  title: string
  icon: string
  price: number
  selected?: boolean
}