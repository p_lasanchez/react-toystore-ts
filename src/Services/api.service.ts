import axios, { AxiosResponse } from 'axios'
import { UserType, ToyType } from './types'

const baseUrl = 'http://localhost:9000'

export const getToys = (): Promise<AxiosResponse<ToyType[]>> => (
  axios.get(`${baseUrl}/toys`)
)

export const authentication = (user: string, pass: string): Promise<AxiosResponse<UserType[]>> => (
  axios.get(`${baseUrl}/users?user=${user}&pass=${pass}`)
)
