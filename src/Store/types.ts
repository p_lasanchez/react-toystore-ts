import { ToyListState } from './toyListReducer/types'
import { AuthState } from './authReducer/types'

export interface RootState {
  toyListReducer: ToyListState
  authReducer: AuthState
}