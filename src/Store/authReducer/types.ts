export enum AUTH {
  OPEN = 'AUTH/OPEN',
  CLOSE = 'AUTH/CLOSE',
  CONNECT = 'AUTH/CONNECT',
  CONNECT_ERROR = 'AUTH/CONNECT_ERROR',
}

export interface AuthState {
  isConnected: boolean
  isOpened: boolean
  isInvalid: boolean
}
