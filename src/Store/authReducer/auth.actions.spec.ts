import * as services from '../../Services/api.service'
import * as actions from './auth.actions'
import { AUTH } from './types'

describe('auth.actions', () => {
  let dispatch

  beforeEach(() => {
    dispatch = jest.fn()
  })

  it('should open', () => {
    const result = actions.openAction()
    expect(result).toEqual({
      type: AUTH.OPEN,
    })
  })

  it('should close', () => {
    const result = actions.closeAction()
    expect(result).toEqual({
      type: AUTH.CLOSE,
    })
  })

  it('should connect with error', () => {
    Object.defineProperty(services, 'authentication', { value: jest.fn(() => Promise.resolve({ data: [] })) })
    const result = actions.connectAction('toto', 'pan')
    result(dispatch).then(() => {
      expect(dispatch).toBeCalledWith({
        type: AUTH.CONNECT_ERROR,
      })
    })
  })

  it('should connect with success', () => {
    Object.defineProperty(services, 'authentication', { value: jest.fn(() => Promise.resolve({ data: ['result'] })) })
    const result = actions.connectAction('toto', 'pan')
    result(dispatch).then(() => {
      expect(dispatch).toBeCalledWith({
        type: AUTH.CONNECT,
      })
    })
  })
})
