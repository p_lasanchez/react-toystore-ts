import { Action } from 'redux'
import { AUTH, AuthState } from './types'

export const defaultAuthState: AuthState = {
  isConnected: false,
  isOpened: false,
  isInvalid: false,
}

const authReducer = (state: AuthState = defaultAuthState, action: Action): AuthState => {
  switch (action.type) {
    case AUTH.OPEN:
      return { ...state, isOpened: true, isInvalid: false }

    case AUTH.CLOSE:
      return { ...state, isOpened: false }

    case AUTH.CONNECT:
      return { ...state, isConnected: true }

    case AUTH.CONNECT_ERROR:
      return { ...state, isInvalid: true }

    default:
      return state
  }
}

export default authReducer
