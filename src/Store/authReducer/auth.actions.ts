import { Dispatch, Action } from 'redux'
import { AxiosResponse } from 'axios'
import { authentication } from '../../Services/api.service'
import { AUTH } from './types'
import { UserType } from '../../Services/types'

export const openAction = (): Action => ({
  type: AUTH.OPEN,
})

export const closeAction = (): Action => ({
  type: AUTH.CLOSE,
})

export const connectAction = (user: string, pass: string) => (dispatch: Dispatch<Action>) => (
  authentication(user, pass).then((res: AxiosResponse<UserType[]>) => {
    if (!res.data.length) {
      dispatch({ type: AUTH.CONNECT_ERROR })
      return
    }

    dispatch({ type: AUTH.CONNECT })
  })
)
