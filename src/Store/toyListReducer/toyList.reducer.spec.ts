import toyListReducer, { defaultToyListState } from './toyList.reducer'
import { TOYS, ToyAction } from './types'
import { ToyType } from '../../Services/types'

describe('toyListReducer', () => {
  const toys: ToyType[] = [
    {
      id: 3, title: 'hop', icon: 'icon', price: 10
    }
  ]

  it('should return default state', () => {
    
    const result = toyListReducer({ ...defaultToyListState }, {
      type: 'fake',
    } as unknown as ToyAction)

    expect(result).toEqual(defaultToyListState)
  })

  it('should return toys', () => {
    const result = toyListReducer({ ...defaultToyListState }, {
      type: TOYS.GET_TOYS_SUCCESS,
      toys,
    })

    expect(result).toEqual({
      ...defaultToyListState,
      toys,
      isLoaded: true,
    })
  })

  it('should select toy', () => {
    const fakeState = {
      ...defaultToyListState,
      toys,
    }
    const result = toyListReducer(fakeState, {
      type: TOYS.SELECT_TOY,
      id: 3,
    })

    expect(result).toEqual({
      ...defaultToyListState,
      toys: [{
        id: 3, title: 'hop', icon: 'icon', price: 10, selected: true
      }],
    })
  })
})
