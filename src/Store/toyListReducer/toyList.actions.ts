import { Dispatch } from 'redux'
import { AxiosResponse } from 'axios'
import { getToys } from '../../Services/api.service'
import { ToyAction, TOYS } from './types'
import { ToyType } from '../../Services/types'

export const populateToysAction = (toys: ToyType[]): ToyAction => ({
  type: TOYS.GET_TOYS_SUCCESS,
  toys
})

export const selectToyAction = (id: number): ToyAction => ({
  type: TOYS.SELECT_TOY,
  id,
})

export const getToysAction = () => (dispatch: Dispatch) => (
  getToys().then((response: AxiosResponse<ToyType[]>) => {
    dispatch(populateToysAction(response.data))
  })
)
