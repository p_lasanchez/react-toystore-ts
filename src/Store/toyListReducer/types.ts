import { ToyType } from "../../Services/types"

export enum TOYS {
  GET_TOYS_SUCCESS = 'TOYS/GET_TOYS_SUCCESS',
  SELECT_TOY = 'TOYS/SELECT_TOY',
}

export interface ToyListState {
  toys: ToyType[]
  isLoaded: boolean
}

interface PopulateToyAction {
  type: TOYS.GET_TOYS_SUCCESS
  toys: ToyType[]
}
interface SelectToyAction {
  type: TOYS.SELECT_TOY
  id: number
}

export type ToyAction = PopulateToyAction | SelectToyAction
