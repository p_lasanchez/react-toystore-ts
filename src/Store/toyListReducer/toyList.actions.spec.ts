import * as services from '../../Services/api.service'
import * as actions from './toyList.actions'
import { TOYS } from './types'

describe('toyList.actions', () => {
  it('should select toy', () => {
    const result = actions.selectToyAction(1)
    expect(result).toEqual({
      type: TOYS.SELECT_TOY,
      id: 1,
    })
  })

  it('should get toys', () => {
    const dispatch = jest.fn()
    const data = [
      { id: 1 }, { id: 2 },
    ]
    Object.defineProperty(services, 'getToys', { value: jest.fn(() => Promise.resolve({ data })) })

    const result = actions.getToysAction()

    return result(dispatch).then(() => {
      expect(dispatch).toBeCalledWith({
        type: TOYS.GET_TOYS_SUCCESS,
        toys: data,
      })
    })
  })
})
