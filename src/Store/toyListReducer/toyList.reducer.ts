import { ToyListState, ToyAction, TOYS } from './types'
import { ToyType } from '../../Services/types'

export const defaultToyListState: ToyListState = {
  toys: [],
  isLoaded: false,
}

const toyListReducer = (state: ToyListState = defaultToyListState, action: ToyAction): ToyListState => {
  let nextToys: ToyType[] = []

  switch (action.type) {
    case TOYS.GET_TOYS_SUCCESS:
      return { ...state, toys: action.toys, isLoaded: true }

    case TOYS.SELECT_TOY:
      nextToys = Array.from(state.toys)

      return {
        ...state,
        toys: nextToys.map((toy: ToyType) => {
          const nextToy:ToyType = toy
          if (nextToy.id === action.id) {
            nextToy.selected = !nextToy.selected
          }
          return nextToy
        }),
      }

    default:
      return state
  }
}

export default toyListReducer
