import { combineReducers, Reducer } from 'redux'

import authReducer, { defaultAuthState } from './authReducer/auth.reducer'
import toyListReducer, { defaultToyListState } from './toyListReducer/toyList.reducer'
import { RootState } from './types'

export const defaultRootState: RootState = {
  toyListReducer: defaultToyListState,
  authReducer: defaultAuthState,
}

const rootReducer: Reducer<RootState> = combineReducers({
  toyListReducer,
  authReducer,
})

export default rootReducer
